﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TiendasServicios.Api.Autor.Modelo;

namespace TiendasServicios.Api.Autor.Pesistencia
{
    public class ContextAutor:DbContext
    {
        public ContextAutor(DbContextOptions<ContextAutor> options):base(options)
        {

        }

        public DbSet<AutorLibro> AutorLibro { get; set; }
        public DbSet<GradoAcademico> GradoAcademico { get; set; }

    }
}
