﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TiendasServicios.Api.Autor.Modelo;
using TiendasServicios.Api.Autor.Negocio;
using TiendasServicios.Api.Autor.Negocio.Dto;

namespace TiendasServicios.Api.Autor.Controllers
{
    //endpoints
    [Route("api/[controller]")]
    [ApiController]
    public class AutorController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AutorController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> CrearAutor(Nuevo.Ejecuta model)
        {
            return await _mediator.Send(model);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AutorDto>> ObtenerAutor(string id)
        {
            return await _mediator.Send(new ConsultaFiltro.AutorUnico {  ArticuloAutorGuid = id });
        }

        [HttpGet]
        public async Task<ActionResult<List<AutorDto>>> ObtenerAutores()
        {
            return await _mediator.Send(new Consulta.ListaAutor());
        }
    }
}
