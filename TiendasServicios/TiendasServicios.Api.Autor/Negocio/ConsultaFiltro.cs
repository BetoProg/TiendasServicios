﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TiendasServicios.Api.Autor.Modelo;
using TiendasServicios.Api.Autor.Negocio.Dto;
using TiendasServicios.Api.Autor.Pesistencia;

namespace TiendasServicios.Api.Autor.Negocio
{
    public class ConsultaFiltro
    {
        public class AutorUnico : IRequest<AutorDto>
        {
            public string ArticuloAutorGuid { get; set; }
            
        }

        public class Manejador : IRequestHandler<AutorUnico, AutorDto>
        {
            private readonly ContextAutor _context;
            private readonly IMapper _mapper;

            public Manejador(ContextAutor context,IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<AutorDto> Handle(AutorUnico request, CancellationToken cancellationToken)
            {
                var autor = await _context.AutorLibro
                    .FirstOrDefaultAsync(x=>x.AutorLibroGuid == request.ArticuloAutorGuid);

                if(autor == null)
                {
                    throw new Exception("No se encontro el autor");
                }

                var autorMapper = _mapper.Map<AutorLibro, AutorDto>(autor);

                return autorMapper;
            }
        }
    }
}
