﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TiendasServicios.Api.Autor.Modelo;
using TiendasServicios.Api.Autor.Negocio.Dto;
using TiendasServicios.Api.Autor.Pesistencia;

namespace TiendasServicios.Api.Autor.Negocio
{
    public class Consulta
    {
        public class ListaAutor : IRequest<List<AutorDto>>
        {
            
        }

        public class Manejador : IRequestHandler<ListaAutor, List<AutorDto>>
        {
            private readonly ContextAutor _context;
            private readonly IMapper _mapper;

            public Manejador(ContextAutor context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<List<AutorDto>> Handle(ListaAutor request, CancellationToken cancellationToken)
            {
                var autores = await _context.AutorLibro.ToListAsync();
                var autorMapper = _mapper.Map<List<AutorLibro>, List<AutorDto>>(autores);
                return autorMapper;
            }
        }
    }
}
