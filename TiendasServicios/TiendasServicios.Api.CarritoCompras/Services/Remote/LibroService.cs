﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using TiendasServicios.Api.CarritoCompras.Interfaces.Remote;
using TiendasServicios.Api.CarritoCompras.Modelo.Remote;

namespace TiendasServicios.Api.CarritoCompras.Services.Remote
{
    public class LibroService : ILibroService
    {
        private readonly IHttpClientFactory _httpClient;
        private readonly ILogger<LibroService> _logger;

        public LibroService(IHttpClientFactory httpClient, ILogger<LibroService> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }
        public async Task<(bool resultado, LibroRmte libro, string ErrrorMessage)> GetLibro(Guid LibroId)
        {
            try
            {
                var cliente = _httpClient.CreateClient("Libros");
                var response = await cliente.GetAsync($"api/Libro/{LibroId}");

                if (response.IsSuccessStatusCode)
                {
                    var contenido = await response.Content.ReadAsStringAsync();
                    var options = new JsonSerializerOptions() 
                    { 
                        PropertyNameCaseInsensitive = true
                    };

                    var result = JsonSerializer.Deserialize<LibroRmte>(contenido,options);
                    return (true, result,null);
                }
                return (false, null, response.ReasonPhrase);
            }
            catch (Exception ex)
            {
                _logger?.LogError(ex.ToString());

                return (false, null,ex.Message);
            }
        }
    }
}
