﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TiendasServicios.Api.CarritoCompras.Negocio.Dto
{
    public class CarritoDto
    {
        public int CarritoId { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public List<CarritoCompraDetalleDto> ListaProductos { get; set; }
    }
}
