﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TiendasServicios.Api.CarritoCompras.Interfaces.Remote;
using TiendasServicios.Api.CarritoCompras.Negocio.Dto;
using TiendasServicios.Api.CarritoCompras.Persistencia;

namespace TiendasServicios.Api.CarritoCompras.Negocio
{
    public class Consulta
    {
        public class Ejecuta : IRequest<CarritoDto>
        {
            public int CarritoSesionId { get; set; }
        }
        public class Manejador : IRequestHandler<Ejecuta, CarritoDto>
        {
            private readonly ContextoCarrito _context;
            private readonly ILibroService _service;

            public Manejador(ContextoCarrito context, ILibroService service)
            {
                _context = context;
                _service = service;
            }
            public async Task<CarritoDto> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                var carritoSesion = await _context.CarritoSesion.FirstOrDefaultAsync(x=>x.CarritoSesionId == request.CarritoSesionId);

                var carritoSesionDetalle = await _context.CarritoSesionDetalle
                    .Where(x => x.CarritoSesionId == request.CarritoSesionId).ToListAsync();

                var ListaCarritoDto = new List<CarritoCompraDetalleDto>();

                foreach (var libro in carritoSesionDetalle)
                {
                   var response = await _service.GetLibro(new Guid(libro.ProductoSeleccionado));

                    if (response.resultado)
                    {
                        var objetLibro = response.libro;

                        var carritoDetalle = new CarritoCompraDetalleDto()
                        {
                            TituloLibro = objetLibro.Titulo,
                            FechaPublicacion = objetLibro.FechaPublicacion,
                            LibroId = objetLibro.LibreriaMaterialId
                        };
                        ListaCarritoDto.Add(carritoDetalle);

                        
                    }
                }

                var carritoSesionDto = new CarritoDto
                {
                    CarritoId = carritoSesion.CarritoSesionId,
                    FechaCreacion = carritoSesion.FechaCreacion,
                    ListaProductos = ListaCarritoDto
                };
                return carritoSesionDto;
            }
        }
    }
}
