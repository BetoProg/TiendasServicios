﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TiendasServicios.Api.CarritoCompras.Modelo.Remote;

namespace TiendasServicios.Api.CarritoCompras.Interfaces.Remote
{
    public interface ILibroService
    {
        Task<(bool resultado, LibroRmte libro, string ErrrorMessage)> GetLibro(Guid LibroId);
    }
}
