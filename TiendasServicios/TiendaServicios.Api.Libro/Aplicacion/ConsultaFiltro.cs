﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TiendaServicios.Api.Libro.Aplicacion.Dto;
using TiendaServicios.Api.Libro.Medelo;
using TiendaServicios.Api.Libro.Persistenccia;

namespace TiendaServicios.Api.Libro.Aplicacion
{
    public class ConsultaFiltro
    {
        public class LibroUnico : IRequest<LibroMaterialDto>
        {
            public int LibroId { get; set; }

        }

        public class Manejador : IRequestHandler<LibroUnico, LibroMaterialDto>
        {
            private readonly ContextLibreia _context;
            private readonly IMapper _mapper;

            public Manejador(ContextLibreia context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<LibroMaterialDto> Handle(LibroUnico request, CancellationToken cancellationToken)
            {
                var libro = await _context.LibreriaMaterial
                    .FirstOrDefaultAsync(x => x.LibreriaMaterialId == request.LibroId);

                if (libro == null)
                {
                    throw new Exception("No se encontro el libro seleccionado");
                }

                var autorMapper = _mapper.Map<LibreriaMaterial, LibroMaterialDto>(libro);

                return autorMapper;
            }
        }
    }
}
