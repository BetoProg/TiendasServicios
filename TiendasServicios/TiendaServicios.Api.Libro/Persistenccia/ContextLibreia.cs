﻿using Microsoft.EntityFrameworkCore;
using TiendaServicios.Api.Libro.Medelo;

namespace TiendaServicios.Api.Libro.Persistenccia
{
    public class ContextLibreia:DbContext
    {
        public ContextLibreia(){}
        public ContextLibreia(DbContextOptions<ContextLibreia> options):base(options)
        {

        }

        public virtual DbSet<LibreriaMaterial> LibreriaMaterial { get; set; }
    }
}
